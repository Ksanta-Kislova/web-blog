terraform {
  backend "s3" {
    bucket         = "kislova-django-app-tfstate"
    key            = "django-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "kislova-django-app-tfstate-lock"
  }
}

provider "aws" {
  region  = "eu-central-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
