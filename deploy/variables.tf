variable "prefix" {
  default = "wb"
}

variable "project" {
  default = "web-blog"
}

variable "contact" {
  default = "kisel.skywalker99@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "django-blog-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "298414249165.dkr.ecr.eu-central-1.amazonaws.com/django-app-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "298414249165.dkr.ecr.eu-central-1.amazonaws.com/django-proxy:dev"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
